const fs = require('fs');
const tools = require('../tools/tools.js')

function openDB () {
  let content = fs.readFileSync('./database/db.json', encoding='UTF-8');
  let parsedContent = JSON.parse(content);
  return parsedContent
}

function searchInDB (whatToSearchFor, dataToBeWritten, DBContent) {
  let found = false;
  for (count in DBContent) {
    for (count2 in DBContent[count]) {
      if (count2 === whatToSearchFor) {
        for (count3 = 0; count3 < DBContent[count][count2].length; count3++) {
          if (DBContent[count][count2][count3].website === dataToBeWritten.website) {
            found = true;
            if (DBContent[count][count2][count3].price > dataToBeWritten.price) {
              DBContent[count][count2][count3].price = dataToBeWritten.price;
              DBContent[count][count2][count3].date = dataToBeWritten.date;
              writeInDB(DBContent);
            }
            return found
          }          
        }
      }
    }
  }
  return found
}

function writeInDB (content) {
  content = JSON.stringify(content, null, ' ');
  fs.writeFileSync('./database/db.json', content, encoding='utf8');
}

function addNewProductInDB (productName, productType, productInfo) {
  let DBContent = openDB();
  let count2=0;

  if (DBContent[productType] == undefined) {
    DBContent[productType] = {}; 
  }

  if (DBContent[productType][productName] !== undefined) {
    for (count2=0; count2<DBContent[productType][productName].length;) {
      if (DBContent[productType][productName][count2]) {
        count2++;
      }
    }
    DBContent[productType][productName][count2] = productInfo;
  } else {
    DBContent[productType][productName] = [productInfo];
  }
  writeInDB(DBContent);
}

module.exports = {
  openDB,
  searchInDB,
  writeInDB,
  addNewProductInDB
}