## Product Price History

This app scans for products defined in database/products.js based on the selectors for specific sites defined in database/sites.js and returns the price found. Afterwards, it checks with the database defined in database/db.json if the product exists (and creates it if not) and writes the minimum price found together with date on which it was found and the corresponding site. It also has a functionality which takes a screenshot of the website as proof.

### Prerequisites:
1. Install NodeJs
2. Run **npm install**
3. Add desired products in database/products.js
4. Add websites corresponding to the ones from step 3 in database/sites.js

### To run:
1. Run **node check-products.js**

### Furure implementations/ideas:
1. Eliminate the need for a separate products.js file; Use db.json instead
2. Create an interface for user interaction (for adding/editing/deleting products)
3. Make the app use a proper DB instead of a file
4. Create a timeline/chart for each product/site in order to see the price evolution