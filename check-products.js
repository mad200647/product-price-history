const products = require('./database/products.js');
const { getPriceFromPage } = require('./tools/manipulate-page.js');
const controller = require('./controller/dbcontroller.js')
const puppeteer = require('puppeteer');
const fs = require('fs');
const { isNull } = require('util');

async function checkProducts () {
  let currentDay = new Date;
  let currentDayFormatted = currentDay.getDate() + '-' + (currentDay.getMonth() + 1) + '-' + currentDay.getFullYear();
  let screenshotLocation = './screenshots/' + currentDayFormatted;
  if (!fs.existsSync(screenshotLocation))
    fs.mkdirSync(screenshotLocation);
  const browser = await puppeteer.launch();
  for (let productType in products) {
    for (let count = 0; count < products[productType].length; count++) {
      for (let count2 = 0; count2 < products[productType][count].links.length; count2++) {
        let result = await getPriceFromPage(products[productType][count].links[count2], products[productType][count].name, browser, screenshotLocation);
        console.log(products[productType][count].name + ': ' + result.minPrice + ' - ' + result.websiteName);
        
        let productName = products[productType][count].name;
        let objectToBeWritten = { price: result.minPrice, website: result.websiteName, date: currentDayFormatted };
        if (!isNull(objectToBeWritten.price)) {
          let found = controller.searchInDB (productName, objectToBeWritten, controller.openDB())
          if (found === false) {
            controller.addNewProductInDB(productName, productType, objectToBeWritten)
          }
        }        
      }
    }
  }      
  await browser.close();
}

checkProducts();