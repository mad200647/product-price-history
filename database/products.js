module.exports = {
  jocuriDeSocietate: [
    {
      name: '7 Wonders Duel',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/7-wonders-duel-promo-messe-editie-in-limba-romana-p-3132.html',
        'https://www.redgoblin.ro/boardgames/7-wonders-duel-editie-in-limba-romana.html',
        'https://www.lexshop.ro/board-games/7-wonders-duel-p11322'
      ]
    },
    {
      name: 'For Sale',
      links: [
        'http://www.regatuljocurilor.ro/in-familie-c-1/for-sale-english-edition-p-485.html'
      ]
    },
    {
      name: 'Dice City',
      links: [
        'http://www.regatuljocurilor.ro/in-familie-c-1/dice-city-p-2123.html',
        'https://www.redgoblin.ro/boardgames/dice-city.html'
      ]
    },
    {
      name: 'Dragon Castle',
      links: [
        'http://www.regatuljocurilor.ro/in-familie-c-1/dragon-castle-p-4697.html',
        'https://www.lexshop.ro/board-games/dragon-castle-p19621'
      ]
    },
    {
      name: 'Santorini',
      links: [
        'http://www.regatuljocurilor.ro/logiceabstracte-c-10/santorini-english-edition-p-7784.html',
        'https://www.redgoblin.ro/boardgames/santorini.html',
        'https://www.lexshop.ro/board-games/santorini-p15366'
      ]
    },
    {
      name: 'Nyet',
      links: [
        'https://www.lexshop.ro/board-games/nyet-p11440'
      ]
    },
    {
      name: 'Alhambra Big Box',
      links: [
        'https://www.redgoblin.ro/boardgames/alhambra-big-box.html',
      ]
    },
    {
      name: 'Raptor',
      links: [
        'https://www.redgoblin.ro/boardgames/raptor.html',
        'http://www.regatuljocurilor.ro/in-familie-c-1/raptor-p-2228.html'
      ]
    },
    {
      name: 'Le Havre',
      links: [
        'https://www.redgoblin.ro/boardgames/le-havre.html',
        'https://www.lexshop.ro/board-games/joc-le-havre-p6905',
        'http://www.regatuljocurilor.ro/strategie-c-2/le-havre-2017-english-edition-p-44.html'
      ]
    },
    {
      name: 'Gizmos',
      links: [
        'https://www.redgoblin.ro/boardgames/gizmos.html',
        'https://www.lexshop.ro/board-games/gizmos-p21951',
        'http://www.regatuljocurilor.ro/in-familie-c-1/gizmos-romanian-edition-p-6203.html',
        'https://carturesti.ro/boardgame/gizmos-400066534',
        'https://www.dol.ro/jucarii/jocuri-de-societate/jocuri-de-societate/joc-gizmos-14y-lex00111.html'
      ]
    },
    {
      name: 'Alhambra',
      links: [
        'https://noriel.ro/joc-de-societate-alhambra',
        'https://www.emag.ro/joc-queen-games-alhambra-9001890791390/pd/DG3BP8BBM',
        'https://www.dol.ro/jucarii/jocuri-si-jucarii-creative/jocuri-de-strategie/joc-alhambra-pkt791390.html',
        'http://www.regatuljocurilor.ro/in-familie-c-1/alhambra-romanian-edition-p-5962.html',
        'https://www.lexshop.ro/board-games/alhambra-revised-edition-p27123',
        'https://www.redgoblin.ro/boardgames/alhambra.html'
      ]
    },
    {
      name: 'Dixit Anniversary',
      links: [
        'https://www.redgoblin.ro/boardgames/dixit-10-anniversary.html',
        'https://www.lexshop.ro/board-games/dixit-10th-anniversary-p20652',
        'http://www.regatuljocurilor.ro/extensii-c-16/dixit-anniversary-2nd-edition-p-6915.html',
        'https://carturesti.ro/boardgame/dixit-10th-anniversary-519958779',
        'https://www.elefant.ro/joc-de-societate-dixit-anniversary_b3a264e0-457a-4e49-a8a3-5652bd2d7d28',
        'https://www.dol.ro/jucarii/jocuri-de-societate/jocuri-de-societate/joc-dixit-anniversary-3-6juc-games00019.html'
      ]
    },
    {
      name: 'Between Two Cities',
      links: [
        'http://www.regatuljocurilor.ro/in-familie-c-1/between-two-cities-p-2855.html',
        'https://www.lexshop.ro/board-games/between-two-cities-p15799'
      ]
    },
    {
      name: 'Between Two Cities - Capitals',
      links: [
        'http://www.regatuljocurilor.ro/extensii-c-16/between-two-cities-capitals-p-3730.html'
      ]
    },
    {
      name: 'Capital',
      links: [
        'https://www.lexshop.ro/board-games/capital-p14488'
      ]
    },
    {
      name: 'Dominion Second Edition',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/dominion-2016-english-second-edition-p-3259.html',
        'https://www.lexshop.ro/board-games/dominion-2nd-edition-p14210',
        'https://www.redgoblin.ro/boardgames/dominion-editia-a-doua.html'
      ]
    },
    {
      name: 'Specter Ops',
      links: [
        'http://www.regatuljocurilor.ro/tematice-c-5/specter-ops-p-1862.html',
        'https://www.redgoblin.ro/boardgames/specter-ops.html'
      ]
    },
    {
      name: 'Reef',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/reef-english-version-p-5350.html',
        'https://www.redgoblin.ro/boardgames/reef.html'
      ]
    },
    {
      name: 'Blue Moon City',
      links: [
        'https://www.redgoblin.ro/boardgames/joc-blue-moon-city.html',
        'http://www.regatuljocurilor.ro/in-familie-c-1/blue-moon-city-p-5791.html'
      ]
    },
    {
      name: 'Tiny Towns',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/tiny-towns-p-6000.html',
        'https://www.lexshop.ro/board-games/tiny-towns-p23119'
      ]
    },
    {
      name: 'Wingspan - Extensia Europeana',
      links: [
        'http://www.regatuljocurilor.ro/extensii-c-16/wingspan-extensia-europeană-p-7671.html',
        'https://www.emag.ro/joc-de-societate-wingspan-european-expansion-85642/pd/DKR1PMMBM'
      ]
    },
    {
      name: 'Atlantis Rising (2nd Edition)',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/atlantis-rising-2nd-edition-p-7259.html'
      ]
    },
    {
      name: 'Stone Age RO',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/stone-age-2017-romanian-edition-p-46.html',
        'https://www.lexshop.ro/board-games/stone-age-p596',
        'https://www.redgoblin.ro/boardgames/stone-age-ediia-in-limba-romana-.html'
      ]
    },
    {
      name: 'Mysterium - Hidden Signs',
      links: [
        'http://www.regatuljocurilor.ro/extensii-c-16/mysterium-hidden-signs-p-2881.html',
        'https://www.redgoblin.ro/boardgames/mysterium-hidden-signs-editie-in-limba-romana.html',
        'https://www.lexshop.ro/board-games/mysterium-hidden-signs-p13186',
        'https://carturesti.ro/boardgame/mysterium-hidden-signs-1661229?p=3'
      ]
    },
    {
      name: 'Mysterium - Secrets and Lies',
      links: [
        'http://www.regatuljocurilor.ro/extensii-c-16/mysterium-secrets-lies-p-4148.html',
        'https://www.redgoblin.ro/boardgames/mysterium-secrets-lies.html',
        'https://www.lexshop.ro/board-games/mysterium-secrets-and-lies-p17764'
      ]
    },
    {
      name: 'Copenhagen',
      links: [
        'https://www.redgoblin.ro/boardgames/joc-copenhagen.html',
        'https://www.lexshop.ro/board-games/copenhagen-p22893'
      ]
    },
    {
      name: 'Blokus',
      links: [
        'http://www.regatuljocurilor.ro/logiceabstracte-c-10/blokus-p-351.html',
        'https://www.redgoblin.ro/boardgames/blokus.html'
      ]
    },
    {
      name: 'Alias',
      links: [
        'http://www.regatuljocurilor.ro/petrecere-c-3/alias-editia-2016-in-limba-romana-p-829.html',
        'https://www.lexshop.ro/board-games/alias-p3419',
        'https://www.redgoblin.ro/boardgames/alias-original.html',
        'https://carturesti.ro/boardgame/alias-1033262',
        'https://www.elefant.ro/joc-alias-original-limba-romana_a805574c-fc48-4b5a-9536-f358f0720484'
      ]
    },
    {
      name: 'Azul',
      links: [
        'https://www.lexshop.ro/board-games/azul-p19670',
        'http://www.regatuljocurilor.ro/logiceabstracte-c-10/azul-2018-romanian-edition-p-4484.html',
        'https://carturesti.ro/boardgame/azul-49180935?p=1',
        'https://www.elefant.ro/joc-azul_9e1551d1-a48f-4995-83cf-954fdfba96de',
        'https://www.redgoblin.ro/boardgames/azul-ediia-in-limba-romana.html'
      ]
    },
    {
      name: 'Barenpark - The Bad News Bears',
      links: [
        'http://www.regatuljocurilor.ro/extensii-c-16/b%C3%A4renpark-the-bad-news-bears-p-6164.html',
      ]
    },
    {
      name: 'Caverna - The Cave Farmers',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/caverna-the-cave-farmers-p-483.html',
        'https://www.lexshop.ro/board-games/caverna-the-cave-farmers-p4494',
        'https://www.redgoblin.ro/boardgames/caverna-the-cave-farmers.html'
      ]
    },
    {
      name: 'The Castles of Burgundy',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/the-castles-of-burgundy-20th-anniversary-p-6522.html',
      ]
    },
    {
      name: 'King of Tokyo - Dark Edition',
      links: [
        'http://www.regatuljocurilor.ro/in-familie-c-1/king-of-tokyo-dark-edition-p-7717.html',
      ]
    },
    {
      name: 'Decrypto',
      links: [
        'http://www.regatuljocurilor.ro/petrecere-c-3/decrypto-romanian-edition-p-6146.html',
        'https://www.lexshop.ro/board-games/decrypto-p21769',
        'https://www.dol.ro/jucarii/jocuri-de-societate/jocuri-de-societate/joc-decrypto-12y-lex00109.html',
        'https://www.elefant.ro/joc-decrypto_5f822a46-1eee-4bb4-b783-b5077ca911e8',
        'https://carturesti.ro/boardgame/decrypto-372710644'
      ]
    },
    {
      name: 'Last Will',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/last-will-english-edition-promo-messenger-p-601.html',
        'https://www.redgoblin.ro/boardgames/last-will.html',
        'https://www.lexshop.ro/board-games/last-will-p3590'
      ]
    },
    {
      name: 'The Pursuit of Happiness',
      links: [
        'http://www.regatuljocurilor.ro/strategie-c-2/the-pursuit-of-happiness-2016-english-second-edition-promo-p-2137.html',
        'https://www.redgoblin.ro/boardgames/the-pursuit-of-happiness.html'
      ]
    },
    {
      name: 'Quirky Circuits',
      links: [
        'https://www.lexshop.ro/board-games/quirky-circuits-p23106'
      ]
    },
  ],
  electronice: [
    {
      name: 'Nikon 10-20mm Obiectiv Foto DSLR',
      links: [
        'https://www.f64.ro/nikon-10-20mm-f-4-5-5-6-af-p-g-vr-dx/p'
      ]
    },
    {
      name: 'Bose SoundLink Mini',
      links: [
        'https://www.emag.ro/boxa-portabila-bose-soundlink-mini-bluetooth-series-ii-argintiu-835799-0200/pd/D630HGBBM/'
      ]
    },
    {
      name: 'BLU-RAY writer extern ASUS SBW-06D2X-U',
      links: [
        'https://www.emag.ro/blu-ray-writer-extern-asus-sbw-06d2x-u-6x-suport-m-disc-compatibil-cu-windows-si-mac-os-asus-webstorage-gratuit-12-luni-nero-backitup-negru-sbw-06d2x-u-blk-g-as/pd/D7Q9RBBBM'
      ]
    },
    {
      name: 'Chromecast Ultra',
      links: [
        'https://www.emag.ro/google-chromecast-ultra-4k-negru-ccultra/pd/D3JZ07BBM'
      ]
    },
    {
      name: 'Casti Bose QuietComfort 35 II Argintiu',
      links: [
        'https://www.avstore.ro/casti/bose-quietcomfort-35-ii/#argintiu',
        'https://www.pcgarage.ro/casti/bose/quietcomfort-35-ii-silver/'
      ]
    },
    {
      name: 'Casti Jabra Elite Active 75t',
      links: [
        'https://www.emag.ro/casti-bluetooth-jabra-elite-active-75t-dark-grey-187199/pd/DMPT7YMBM',
        'https://www.avstore.ro/casti/jabra-elite-active-75t/',
        'https://altex.ro/casti-jabra-elite-active-75t-true-wireless-bluetooth-in-ear-microfon-noise-cancelling-titanium-black/cpd/CASELITEAC75TTI/'
      ]
    },
    {
      name: 'Casti On-Ear A+ SBG1 Wireless',
      links: [
        'https://www.emag.ro/casti-audio-bluetooth-a-negru-sbg1/pd/DQBR72BBM'
      ]
    },
    {
      name: 'Filtru antibacterian pentru Purificator aer Xiaomi Violet',
      links: [
        'https://www.pcgarage.ro/filtrare-aer/xiaomi/filtru-antibacterial-pentru-purificator-aer-xiaomi/'
      ]
    },
    {
      name: 'Acumulatori Duracell AAK4 R6 2500mAh, 4 buc',
      links: [
        'https://www.emag.ro/acumulatori-duracell-aak4-r6-2500mah-4-buc-81418263/pd/D7P0LBBBM'
      ]
    },
    {
      name: 'Acumulatori A+ 2600mAh, AA 4 buc',
      links: [
        'https://www.emag.ro/acumulatori-a-2600mah-aa-4-buc-nbpaa/pd/D4L26MBBM'
      ]
    },
    {
      name: 'Acumulatori A+ 1000mAh AAA 4 buc',
      links: [
        'https://www.emag.ro/acumulatori-a-1000mah-aaa-4-buc-nbpaaa/pd/DVL26MBBM'
      ]
    },
    {
      name: 'Oral-B PRO 2 2000 Cross Action',
      links: [
        'https://www.emag.ro/periuta-de-dinti-electrica-oral-b-pro-2-2000-cross-action-40000-pulsatii-min-8800-oscilatii-min-curatare-3d-2-programe-1-capat-alb-albastru-4210201135012/pd/DPNMZYBBM'
      ]
    },
    {
      name: 'Oral-B Sensitive Ultra Thin',
      links: [
        'https://www.emag.ro/rezerva-periuta-de-dinti-oral-b-2-buc-sensitive-ultra-thin-4210201176596/pd/DJQ4LNBBM/'
      ]
    },
    {
      name: 'Creative Sound Blaster X-Fi Surround 5.1 Pro v3',
      links: [
        'https://www.emag.ro/placa-de-sunet-creative-sound-blaster-x-fi-surround-5-1-pro-v3-interfata-usb-5-1-canale-70sb109500008/pd/DBB75LBBM/'
      ]
    },
    {
      name: 'Thermaltake Core P3 Snow Edition',
      links: [
        'https://www.vexio.ro/carcase-pc/thermaltake/472876-thermaltake-core-p3-snow-edition-white-with-window-ca-1g4-00m6wn-00/'
      ]
    },
    {
      name: 'Zotac GeForce RTX 3060 Ti AMP White Edition LHR 8GB GDDR6 256-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/zotac/geforce-rtx-3060-ti-amp-white-edition-lhr-8gb-gddr6-256-bit/'
      ]
    },
    {
      name: 'GIGABYTE GeForce RTX 3060 Ti VISION OC LHR 8GB GDDR6 256-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/gigabyte/geforce-rtx-3060-ti-vision-oc-lhr-8gb-gddr6-256-bit/'
      ]
    },
    {
      name: 'GIGABYTE GeForce RTX 3070 VISION OC LHR 8GB GDDR6 256-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/gigabyte/geforce-rtx-3070-vision-oc-lhr-8gb-gddr6-256-bit/'
      ]
    },
    {
      name: 'Zotac GeForce RTX 3070 Twin Edge OC White Edition 8GB GDDR6 256-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/zotac/geforce-rtx-3070-twin-edge-oc-white-edition-8gb-gddr6-256-bit/'
      ]
    },
    {
      name: 'ASUS GeForce RTX 3070 ROG STRIX O8G White 8GB GDDR6 256-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/asus/geforce-rtx-3070-rog-strix-o8g-white-8gb-gddr6-256-bit/'
      ]
    },
    {
      name: 'GIGABYTE GeForce RTX 3080 VISION OC LHR 10GB GDDR6X 320-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/gigabyte/geforce-rtx-3080-vision-oc-lhr-10gb-gddr6x-320-bit/'
      ]
    },
    {
      name: 'ASUS GeForce RTX 3080 ROG STRIX O10G White LHR 10GB GDDR6X 320-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/asus/geforce-rtx-3080-rog-strix-o10g-white-lhr-10gb-gddr6x-320-bit/'
      ]
    },
    {
      name: 'GIGABYTE GeForce RTX 3080 VISION OC 10GB GDDR6X 320-bit',
      links: [
        'https://www.pcgarage.ro/placi-video/gigabyte/geforce-rtx-3080-vision-oc-10gb-gddr6x-320-bit/'
      ]
    },
  ]
}