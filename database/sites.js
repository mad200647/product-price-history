module.exports = {
  sites: [
    // Diverse
    {
      websiteName: 'Altex',
      websiteAddress: 'https://altex.ro/',
      websitePriceSelectors: ['.product-shop .Price-int']
    },
    {
      websiteName: 'Media Galaxy',
      websiteAddress: 'https://mediagalaxy.ro/',
      websitePriceSelectors: ['.Price-current .Price-int', '.product-shop .Price-int']
    },
    {
      websiteName: 'Domo',
      websiteAddress: 'https://www.domo.ro/',
      websitePriceSelectors: ['#product-price']
    },
    {
      websiteName: 'CEL',
      websiteAddress: 'https://www.cel.ro/',
      websitePriceSelectors: ['#product-price']
    },
    {
      websiteName: 'elefant',
      websiteAddress: 'https://www.elefant.ro/',
      websitePriceSelectors: ['.pdp-table [data-testing-id="current-price"]']
    },
    {
      websiteName: 'eMag',
      websiteAddress: 'https://www.emag.ro/',
      websitePriceSelectors: ['.main-product-form .product-new-price', '.main-product-form  .product-old-price']
    },
    {
      websiteName: 'evoMag',
      websiteAddress: 'https://www.evomag.ro/',
      websitePriceSelectors: ['.price_ajax .pret_rons', '.price_ajax .pret_taiat']
    },
    {
      websiteName: 'PC Garage',
      websiteAddress: 'https://www.pcgarage.ro/',
      websitePriceSelectors: ['#psbx .ps_sell_price .price_num', '#psbx .ps_price_old span']
    },
    {
      websiteName: 'Vexio',
      websiteAddress: 'https://www.vexio.ro/',
      websitePriceSelectors: ['#price-value']
    },
    {
      websiteName: 'Shop4PC',
      websiteAddress: 'http://www.shop4pc.ro/',
      websitePriceSelectors: ['.main tr td b font[size="5px"]']
    },
    // audio-foto-video
    {
      websiteName: 'F64',
      websiteAddress: 'https://www.f64.ro/',
      websitePriceSelectors: ['.price-info .skuBestPrice', '.vtex-flex-layout-0-x-flexColChild.vtex-flex-layout-0-x-flexColChild--productRightRight.pb0']
    },
    {
      websiteName: 'AVstore',
      websiteAddress: 'https://www.avstore.ro/',
      websitePriceSelectors: ['.pret-nou']
    },  
    // jocuri de societate
    {
      websiteName: 'Red Goblin',
      websiteAddress: 'https://www.redgoblin.ro/',
      websitePriceSelectors: ['.current-price span', '.regular-price']
    },
    {
      websiteName: 'Regatul Jocurilor',
      websiteAddress: 'http://www.regatuljocurilor.ro/',
      websitePriceSelectors: ['.normalprice', '.productSpecialPrice', '#productPrices [itemprop="price"]']
    },
    {
      websiteName: 'Carturesti',
      websiteAddress: 'https://carturesti.ro/',
      websitePriceSelectors: ['.pret']
    },
    {
      websiteName: 'Diverta',
      websiteAddress: 'https://www.dol.ro/',
      websitePriceSelectors: ['#product_price']
    },
    {
      websiteName: 'Lex Shop',
      websiteAddress: 'https://www.lexshop.ro/',
      websitePriceSelectors: ['.main_price']
    },
    {
      websiteName: 'Noriel',
      websiteAddress: 'https://noriel.ro/',
      websitePriceSelectors: ['.product-container-right .price-final_price .price']
    }
  ]
}