const tools = require('./tools.js');

async function getPriceFromPage (link, productName, browser, screenshotLocation) {
  let minPrice = Infinity;
  let functionResult = tools.getSiteFromLink(link);
  let selectorsFromWebsite = functionResult.selectors;
  let websiteName = functionResult.name;
  const page = await browser.newPage();
  await page.setViewport({
    width: 1200,
    height: 1000,
    deviceScaleFactor: 1,
  });
  try {
    await page.goto(link);
  }
  catch (error) {
    console.log(error)
    return {minPrice, websiteName}
  }  
  await page.screenshot({path: `${screenshotLocation}/${productName} - ${websiteName}.jpeg`, type: 'jpeg'});
  for (let count = 0; count < selectorsFromWebsite.length; count++) {
    let selector = selectorsFromWebsite[count];
    let productPrice = await page.evaluate(selector => {
      let priceFromPage = document.querySelector(selector)
      if (priceFromPage) {
        let selectorChild = priceFromPage.lastElementChild;
        while (selectorChild) {
          priceFromPage.removeChild(selectorChild);
          selectorChild = priceFromPage.lastElementChild;
        }
        return priceFromPage.innerText.replace(/[^0-9.,]/g, '')
      }
    }, selector);
    
    if (count === 0 && productPrice) {
      minPrice = productPrice;
    } else if (count > 0 && minPrice > parseInt(productPrice) && productPrice) {
      minPrice = productPrice;
    }
  }

  if (minPrice != Infinity) {
    minPrice = tools.parseResult(minPrice);
  }  
  
  return {minPrice, websiteName}
}

module.exports = {
  getPriceFromPage
}