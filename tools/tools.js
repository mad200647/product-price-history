const sites = require('../database/sites.js')

function getSiteFromLink (link) {
  for (let count = 0; count < sites.sites.length; count++) 
    if (link.includes(sites.sites[count].websiteAddress))  
      return {selectors: sites.sites[count].websitePriceSelectors, 
              name: sites.sites[count].websiteName}
}

function parseResult (price) {
  let commaDifference = price.length - price.indexOf(',');
  let dotDifference = price.length - price.indexOf('.'); 
  let difference = commaDifference - dotDifference; 
  if (difference === 0) {
    return parseInt(price)
  } else if (commaDifference <= 3) {
    price = price.substring(0, price.indexOf(','));
    price = price.replace(/[.]/g,'');
    return parseInt(price)
  } else if (dotDifference <= 3) {
    price = price.substring(0, price.indexOf('.'));
    price = price.replace(/[,]/g,'');
    return parseInt(price)
  } else {
    price = price.replace(/[.]/g,'');
    price = price.replace(/[,]/g,'');
    return parseInt(price)
  }
}

module.exports = {
  getSiteFromLink,
  parseResult
}